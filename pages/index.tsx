import Head from 'next/head'
import Banner from '../components/Banner'
import Header from '../components/Header'
import PostComp from '../components/Post'
import { sanityClient, urlFor } from '../sanity'
import { Post } from '../typings'

interface Props {
  posts: [Post]
}

export default function Home({ posts }: Props) {
  return (
    <div className="max-w-7xl mx-auto">
      <Head>
        <title>Medium Blog</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <Banner />
      {/* Posts */}
      <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-3 md:gap-6 p-2 md:p-6">
        {posts.map(post => (
          <PostComp 
            key={post._id}
            slug={post.slug.current} 
            img={urlFor(post.mainImage).url()!} title={post.title} 
            description={post.description}
            authorName={post.author.name}
            authorImg={urlFor(post.author.image).url()!}
          />
        ))}
      </div>
    </div>
  )
}

export const getServerSideProps = async () => {
  const query = `*[_type == "post"] {
    _id,
    title,
    author -> {
      name,
      image
    },
    description,
    mainImage,
    slug,
  }`;

  const posts = await sanityClient.fetch(query);

  return { 
    props: {
      posts,
    }
  }
}