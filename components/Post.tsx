import Link from 'next/link';
import React, {FC} from 'react';

interface Props {
  slug: string;
  img: string;
  title: string;
  description: string;
  authorName: string;
  authorImg: string;
}

const PostComp: FC<Props> = ({ slug, img, title, description, authorName, authorImg}) => {
  return <Link href={`/post/${slug}`}>
  <div className="border rounded-lg group cursor-pointer overflow-hidden">
    <img className="h-60 w-full object-cover group-hover:scale-105 transition-transform duration-200 ease-in-out" src={img} alt="" />
    <div className="flex justify-between p-5 bg-white">
      <div>
        <p className="text-lg font-bold">{title}</p>
        <p className="text-xs">{description} by {authorName}</p>
      </div>
      <img className="h-12 w-12 rounded-full" src={authorImg} alt="" />
    </div>
  </div>
</Link>;
}

export default PostComp;
